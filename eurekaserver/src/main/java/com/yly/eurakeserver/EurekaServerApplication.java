package com.yly.eurakeserver; /**
 * @copyRight http://www.cqebd.cn
 **/

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @className com.yly.eurakeserver.EurekaServerApplication
 * @description TODO
 * @modify
 * @author YLY
 * @email yuanhongguo@cqebd.cn
 * @date 2018/11/12 14:14
 * @version 1.0
 **/
@SpringBootApplication
@EnableEurekaServer
public class EurekaServerApplication {
    public static  void  main(String[] arg)
    {
        SpringApplication.run(EurekaServerApplication.class,arg);
    }
}
