/**
 * @copyRight http://www.cqebd.cn
 **/
package com.yly.redis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

/**
 * @className RedisApplictaion
 * @description TODO
 * @modify
 * @author YLY
 * @email yuanhongguo@cqebd.cn
 * @date 2018/11/22 14:14
 * @version 1.0
 **/
@SpringBootApplication
@EnableCaching
public class RedisApplication {

    public static void  main(String[] arg)
    {
        SpringApplication.run(RedisApplication.class,arg);
    }
}
