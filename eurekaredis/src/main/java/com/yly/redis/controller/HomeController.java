/**
 * @copyRight http://www.cqebd.cn
 **/
package com.yly.redis.controller;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.yly.redis.utils.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.Serializable;
import java.util.Date;

/**
 * @className HomeController
 * @description TODO
 * @modify
 * @author YLY
 * @email yuanhongguo@cqebd.cn
 * @date 2018/11/22 14:29
 * @version 1.0
 **/
@RestController
public class HomeController {
    @Autowired
    private RedisTemplate<String, Object> redisTemplate;
    @GetMapping("/test")
    public String getMsg() {
        //redisTemplate.opsForValue().set("test",new {id="1",name="test2"});
        testModel testM= new testModel();
        testM.setId("1");
        testM.setName("袁ygh");
        redisTemplate.opsForValue().set("test2","123333-");

        //testModel entity=(testModel)redisTemplate.opsForValue().get("test2");
        return redisTemplate.opsForValue().get("test2")+"123";
    }

    public class  testModel implements Serializable {
        private String id;
        private String  name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
