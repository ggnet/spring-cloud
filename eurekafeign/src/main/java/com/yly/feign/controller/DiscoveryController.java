/**
 * @copyRight http://www.cqebd.cn
 **/
package com.yly.feign.controller;

import com.yly.feign.impl.DisClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @className DiscoveryController
 * @description TODO
 * @modify
 * @author YLY
 * @email yuanhongguo@cqebd.cn
 * @date 2018/11/12 15:33
 * @version 1.0
 **/
@RestController
public class DiscoveryController {
    @Autowired
    DisClient restTemplate;
    @GetMapping("/consumer")
    public String getMsg() {
        return restTemplate.consumer();
    }
}
