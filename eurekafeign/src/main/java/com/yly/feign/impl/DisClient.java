/**
 * @copyRight http://www.cqebd.cn
 **/
package com.yly.feign.impl;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @className DisClient
 * @description TODO
 * @modify
 * @author YLY
 * @email yuanhongguo@cqebd.cn
 * @date 2018/11/12 15:32
 * @version 1.0
 **/
@FeignClient(value = "eureka-client",fallback = DisClientImpl.class)
public interface DisClient {
    @GetMapping("/client")
    String consumer();
}
