package com.yly.feign.impl;

import org.springframework.stereotype.Component;

@Component
public class DisClientImpl implements DisClient {

    @Override
    public String consumer() {
        return "hi,sorry,error!";
    }
}
