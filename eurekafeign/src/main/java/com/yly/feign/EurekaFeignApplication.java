/**
 * @copyRight http://www.cqebd.cn
 **/
package com.yly.feign;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @className EurekaFeignApplication
 * @description TODO
 * @modify
 * @author YLY
 * @email yuanhongguo@cqebd.cn
 * @date 2018/11/12 15:28
 * @version 1.0
 **/
@SpringBootApplication
@EnableFeignClients
@EnableDiscoveryClient
public class EurekaFeignApplication {
    public static void  main(String[] arg)
    {
        SpringApplication.run(EurekaFeignApplication.class,arg);
    }
}
