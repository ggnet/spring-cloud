/**
 * @copyRight http://www.cqebd.cn
 **/
package com.yly.ribbon.controller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.security.auth.callback.CallbackHandler;
import java.sql.Date;
import java.sql.Time;

/**
 * @className RibbonController
 * @description TODO
 * @modify
 * @author YLY
 * @email yuanhongguo@cqebd.cn
 * @date 2018/11/12 15:05
 * @version 1.0
 **/
@RestController
public class RibbonController {
    @Autowired
    RestTemplate restTemplate;

    @HystrixCommand(fallbackMethod = "CallError")
    @GetMapping("/consumer")
    public String getMsg() {
        return restTemplate.getForObject("http://eureka-client/client", String.class);
    }

    public String CallError() {
        return "hi,sorry,error!";
    }
}
