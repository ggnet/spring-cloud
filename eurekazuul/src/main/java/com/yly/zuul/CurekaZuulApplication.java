/**
 * @copyRight http://www.cqebd.cn
 **/
package com.yly.zuul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * @className CurekaZuulApplication
 * @description TODO
 * @modify
 * @author YLY
 * @email yuanhongguo@cqebd.cn
 * @date 2018/11/12 16:31
 * @version 1.0
 **/
@SpringBootApplication
@EnableZuulProxy
@EnableEurekaClient
@EnableDiscoveryClient
public class CurekaZuulApplication {
    public static void  main(String[] arg)
    {
        SpringApplication.run(CurekaZuulApplication.class,arg);
    }
}
