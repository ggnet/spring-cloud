/**
 * @copyRight http://www.cqebd.cn
 **/
package com.yly.eurekaclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @className EurekaClientApplication
 * @description TODO
 * @modify
 * @author YLY
 * @email yuanhongguo@cqebd.cn
 * @date 2018/11/12 14:35
 * @version 1.0
 **/
@SpringBootApplication
@EnableEurekaClient
public class EurekaClientApplication {
    public static void main(String[] arg)
    {
        SpringApplication.run(EurekaClientApplication.class,arg);
    }
}
